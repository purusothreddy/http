const app = angular.module("httpApp", []);

/**
 * the angular object is provided by importing the angular CDN from http://www.angularjs.org.
 * the module method provides a starting point (like a main method) for the modules (or app) 
 * it accepts the name as the first argument. the second argument accepts an array for any external
 * modules to be injected into this module.
 * 
 * the app variable is now assigned the responsibility of the module. 
 */