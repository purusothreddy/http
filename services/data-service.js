app.service("dataService", function ($http) {
    this.getPosts = function () {
        return $http.get("https://jsonplaceholder.typicode.com/posts");
    }

    this.getComments = function (id) {
        return $http.get("https://jsonplaceholder.typicode.com/comments?postId=" + id);
    }
})